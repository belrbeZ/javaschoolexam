package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.duplicates.DuplicateFinder;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator implements ICalculator {
    private static final Logger LOGGER = Logger.getLogger(Calculator.class.getName());

    private static final DecimalFormat formatter = Calculator.initFormatter();

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    @Override
    public String evaluate(String statement) {
        LinkedList<String> postfixExpression;
        try {
            String expression = formatString(statement);
            postfixExpression = makePostfix(expression);
            String result = execute(postfixExpression);
            LOGGER.info("Evaluated: " + statement + "=" +result);
            return result;
        } catch (Exception e) {
            LOGGER.severe("Incorrect expression: " + statement);
            return null;
        }
    }

    /**
     * Formatting input string.
     *
     * @param statement string to be evaluated.
     * @return list of read lines.
     */
    private String formatString(String statement) {
        String reformatted;
        // check string expression
        reformatted = statement.replaceAll(" ", "")
                .replace("(-", "(0-");
        if (reformatted.charAt(0) == '-') {
            reformatted = "0" + reformatted;
        }
        return reformatted;
    }

    /**
     * Creating the stack of expressions to be calculated from string-expression.
     *
     * @param statement string to be evaluated.
     * @throws Exception during parsing string and creating postfixExpression.
     */
    private LinkedList<String> makePostfix (String statement) throws Exception{
        LinkedList<String> postfixExpression = new LinkedList<>();
        LinkedList<String> operationStack = new LinkedList<>();
        StringBuilder number = new StringBuilder();
        for (int i = 0; i < statement.length(); i++) {
            String symbol = String.valueOf(statement.charAt(i));
            String lastOperator;
            switch (Operations.calc(symbol)) {
                case OPEN_PARENTHESIS:
                    addingNumberToPostExpr(number, postfixExpression);
                    operationStack.push(symbol);
                    break;
                case CLOSE_PARENTHESIS:
                    addingNumberToPostExpr(number, postfixExpression);
                    String operator;
                    while (!(operator = operationStack.pop()).equals("(")) {
                        postfixExpression.push(operator);
                    }
                    break;
                case MULTIPLY:
                    addingNumberToPostExpr(number, postfixExpression);
                    lastOperator = operationStack.peek();
                    if (lastOperator == null || lastOperator.equals("+") || lastOperator.equals("-") || lastOperator.equals("(")) {
                        operationStack.push(symbol);
                    } else {
                        if (operationStack.size() != 0) postfixExpression.push(operationStack.pop());
                        operationStack.push(symbol);
                    }
                    break;
                case DIVIDE:
                    addingNumberToPostExpr(number, postfixExpression);
                    lastOperator = operationStack.peek();
                    if (lastOperator == null || lastOperator.equals("+") || lastOperator.equals("-") || lastOperator.equals("(")) {
                        operationStack.push(symbol);
                    } else if (operationStack.size() != 0) postfixExpression.push(operationStack.pop());
                    break;
                case PLUS:
                    addingNumberToPostExpr(number, postfixExpression);
                    lastOperator = operationStack.peek();
                    if (lastOperator != null && !lastOperator.equals("(") && operationStack.size() != 0) {
                        postfixExpression.push(operationStack.pop());
                    }
                    operationStack.push(symbol);
                    break;
                case MINUS:
                    addingNumberToPostExpr(number, postfixExpression);
                    lastOperator = operationStack.peek();
                    if (lastOperator != null && !lastOperator.equals("(") && operationStack.size() != 0) {
                        postfixExpression.push(operationStack.pop());
                    }
                    operationStack.push(symbol);
                    break;
                default:
                    number.append(symbol);
                    if (i + 1 == statement.length() && !number.toString().equals("")) {
                        addingNumberToPostExpr(number, postfixExpression);
                    }
                    break;
            }
        }
        for (int j = 0; j <= operationStack.size(); j++) {
            postfixExpression.push(operationStack.pop());
        }
        return postfixExpression;
    }

    /**
     * Adding number to the stack of numbers and operations, which will be later calculated.
     *
     * @param number new number for calculating.
     * @param expression stack of expression.
     */
    private void addingNumberToPostExpr(StringBuilder number, LinkedList<String> expression) {
        if (!number.toString().equals("")) {
            expression.push(number.toString());
            number.delete(0, number.length());
        }
    }

    /**
     * Calculating value from stack of numbers and operations from input expression.
     *
     * @param expression stack of operations and operands.
     */
    private String execute(LinkedList<String> expression) {
        LinkedList<String> result = new LinkedList<>();
        while(expression.size() > 0) {
            String tmp = expression.pollLast();
            double firstValue, secondValue;
            switch (Operations.calc(tmp)) {
                case MULTIPLY:
                    result.push(String.valueOf(Double.parseDouble(result.pop())*Double.parseDouble(result.pop())));
                    break;
                case DIVIDE:
                    firstValue = Double.parseDouble(result.pop());
                    secondValue = Double.parseDouble(result.pop());
                    if (firstValue == 0) throw new NumberFormatException("Try to divide on zero.");
                    result.push(String.valueOf(secondValue/firstValue));
                    break;
                case PLUS:
                    result.push(String.valueOf(Double.parseDouble(result.pop())+Double.parseDouble(result.pop())));
                    break;
                case MINUS:
                    firstValue = Double.parseDouble(result.pop());
                    secondValue = Double.parseDouble(result.pop());
                    result.push(String.valueOf(secondValue - firstValue));
                    break;
                default:
                    result.push(tmp);
                    break;
            }
        }
        return formatter.format(Double.parseDouble(result.poll()));
    }

    /**
     * Constants for identify type of operation.
     */
    enum  Operations {
        PLUS("+"),
        MINUS("-"),
        MULTIPLY("*"),
        DIVIDE("/"),
        OPEN_PARENTHESIS("("),
        CLOSE_PARENTHESIS(")"),
        OTHER("");

        private final String asStr;

        public String asStr() {
            return asStr;
        }

        private Operations(String asStr) {
            this.asStr = asStr;
        }

        public static Operations calc(String value) {
            switch (value) {
                case "+": return Operations.PLUS;
                case "-": return Operations.MINUS;
                case "*": return Operations.MULTIPLY;
                case "/": return Operations.DIVIDE;
                case "(": return Operations.OPEN_PARENTHESIS;
                case ")": return Operations.CLOSE_PARENTHESIS;
                default: return Operations.OTHER;
            }
        }
    }

    private static DecimalFormat initFormatter() {
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.ENGLISH);
        DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
        decimalFormat.applyPattern("#.####");
        decimalFormat.setDecimalSeparatorAlwaysShown(false);
        return decimalFormat;
    }

}
