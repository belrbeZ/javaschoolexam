package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by AlexVasil on 24.05.2017.
 *
 * @author AlexVasil
 */
/**
 * Created by AlexVasil on 01.02.2017.
 * Interface for Calculator task
 */
public interface ICalculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    String evaluate(String statement);

}