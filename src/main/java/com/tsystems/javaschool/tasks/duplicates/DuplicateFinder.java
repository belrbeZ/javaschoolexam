package com.tsystems.javaschool.tasks.duplicates;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

public class DuplicateFinder implements IDuplicateFinder {

    private static final Logger LOGGER = Logger.getLogger(DuplicateFinder.class.getName());

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    @Override
    public boolean process(File sourceFile, File targetFile) {
        try {
            if(sourceFile==null || targetFile==null) {
                LOGGER.severe("Both files for reading and writing must exist.");
                throw new IllegalArgumentException("Both files for reading and writing must exist.");
            }
            if(!sourceFile.exists()){
                LOGGER.severe("File by path \"" + sourceFile.getPath() + "\" for reading had not been created.");
                throw new IOException("File by path \"" + sourceFile.getPath() + "\" for reading had not been created.");
            }
            if(!sourceFile.canRead()){
                LOGGER.severe("File by path \"" + sourceFile.getPath() + "\" is not available for reading.");
                return false;
            }
            Charset charset = Charset.forName("ISO-8859-1");
            List<String> listStr = readTextFromFile(sourceFile, charset);

            Map<String, Integer> treemap = new TreeMap<>((astr, bstr) -> astr.compareTo(bstr));// Or simply (Comparator.naturalOrder());

            for(String s:listStr) {
                treemap.put(s,Collections.frequency(listStr, s));
            }

            writeMapInFile(treemap, targetFile, charset);
        } catch (IOException e) {
            LOGGER.severe("ERROR: Can't finish operation: " + e);
            return false;
        }
        return true;
    }

    /**
     * Reading data from file.
     *
     * @param sourceFile file to be processed
     * @param charset charset to processed file.
     * @return list of read lines.
     * @throws IOException of reading.
     */
    private List readTextFromFile(File sourceFile, Charset charset) throws IOException {
        List<String> listStr = new ArrayList<>();
        try (BufferedReader br = Files.newBufferedReader(Paths.get(sourceFile.getName()),charset)) {
            String line;
            while ((line = br.readLine()) != null) {
                listStr.add(line);
            }
        } catch (IOException e) {
            LOGGER.severe("Reading of file by path \"" + sourceFile.getPath() + "\" aborted.");
            throw new IOException("Reading of file by path \"" + sourceFile.getPath() + "\" aborted.");
        }
        return listStr;
    }

    /**
     * Writing data to the target file.
     *
     * @param treemap map of processed strings.
     * @param targetFile file for output.
     * @param charset charset to processed file.
     * @throws IOException of writing.
     */
    private void writeMapInFile(Map treemap, File targetFile, Charset charset) throws IOException {
        if (targetFile.exists() && !targetFile.canWrite()) {
            LOGGER.severe("File by path \"" + targetFile.getPath() + "\" is not available for writing");
            throw new IOException("ERROR: File by path \"" + targetFile.getPath() + "\" is not available for writing");
        }

        try (BufferedWriter writer = Files.newBufferedWriter(targetFile.toPath(), charset, CREATE, APPEND)) {
            //получить множество элементов
            StringBuilder str = new StringBuilder("\n");
            Set<Map.Entry<String, Integer>> set = treemap.entrySet();
            for (Map.Entry entr :
                    set) {
                writer.write(entr.getKey() + " [" + entr.getValue() + "]\n");
                str.append(entr.getKey() + " [" + entr.getValue() + "]\n");
                writer.flush();
            }
            LOGGER.info("Lines added to file: "+str);

        } catch (IOException x) {
            LOGGER.severe("Writing in file by path \"" + targetFile.getPath() + "\" aborted.");
            throw new IOException("Writing in file by path \"" + targetFile.getPath() + "\" aborted.");
        }
    }

}
