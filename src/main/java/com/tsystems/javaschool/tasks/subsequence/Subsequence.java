package com.tsystems.javaschool.tasks.subsequence;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

public class Subsequence implements ISubsequence {
    private static final Logger LOGGER = Logger.getLogger(Subsequence.class.getName());

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    @Override
    public boolean find(List x, List y) {
        //Checking for request lists. Are used for optimization.
        if((x==null) || (y==null)) {
            throw new IllegalArgumentException("Both sequences must exist!");
        }
        if(x.isEmpty()){
            return true;
        }
        if(y.isEmpty()){
            return false;
        }
        int size1 = x.size();
        int size2 = y.size();
        if(size1>size2){
            return false;
        }

        //Use listIterator of LinkedList for quicklier searching of elements.
        ListIterator<?> itr1 = new LinkedList(x).listIterator();
        ListIterator<?> itr2 = new LinkedList(y).listIterator();

        Object durList1 = null;
        try {
            x:while(itr1.hasNext()){
                durList1 = itr1.next();
                while (itr2.hasNext()){
                    if(itr2.next().equals(durList1)){
                        continue x;
                    }

                    //If the remaining number of elements of the first list is greater than the remaining number of elements of the second list,
                    // then all the remaining elements of the first list will not be exactly in the second - is incorrect
                    // the number of unidentified elements of the first list  in the second - the number of elements of the second list
                    if((size1 - (itr1.nextIndex()-1)) > (size2-itr2.nextIndex())){
                        LOGGER.info("The first sequence can not be applied to the second.");
                        return false;
                    }
                }
            }
        } catch (NoSuchElementException e) {
            LOGGER.severe("Error in searching.");
        }
        LOGGER.info("The first sequence can be applied to the second!");
        return true;
    }
}
