package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

/**
 * Created by AlexVasil on 23.05.2017.
 *
 *  Interface for Subsequence task
 *
 * @author AlexVasil
 */
public interface ISubsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    boolean find(List x, List y);
}